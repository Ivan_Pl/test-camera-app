package com.pl.cameraapp

import android.app.Application
import com.pl.cameraapp.di.AppComponent
import com.pl.cameraapp.di.DaggerAppComponent
import com.pl.cameraapp.di.modules.AppModule
import timber.log.Timber

class CameraApp : Application() {

    companion object {
        @JvmStatic
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerComponent()
        initTimber()
    }

    private fun initDaggerComponent() {
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}