package com.pl.cameraapp.di

import com.pl.cameraapp.di.modules.AppModule
import com.pl.cameraapp.presentation.camera.CameraFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(fragment: CameraFragment)
}