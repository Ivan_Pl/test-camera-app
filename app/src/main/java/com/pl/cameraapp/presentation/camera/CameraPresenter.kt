package com.pl.cameraapp.presentation.camera

import android.Manifest
import android.content.pm.PackageManager
import com.arellomobile.mvp.InjectViewState
import com.otaliastudios.cameraview.PictureResult
import com.pl.cameraapp.R
import com.pl.cameraapp.data.AppPermissionsRepository
import com.pl.cameraapp.data.CameraFilterRepository
import com.pl.cameraapp.data.PhotoRepository
import com.pl.cameraapp.presentation.base.BasePresenter
import com.pl.cameraapp.presentation.base.model.CameraOptions
import com.pl.cameraapp.ui.swipeHandling.SwipeDirection
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class CameraPresenter @Inject constructor(private val permissionsRepo: AppPermissionsRepository,
                                          private val cameraFiltersRepo: CameraFilterRepository,
                                          private val photoRepository: PhotoRepository) : BasePresenter<CameraMvpView>() {

    private val requiredPermissions by lazy { arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA) }
    private var isPermissionCheckRequired = false

    fun onPermissionsGranted(results: IntArray) {
        val isAllPermissionsGranted = results.none { PackageManager.PERMISSION_GRANTED != it }
        viewState.setTakePhotoBtnVisible(isAllPermissionsGranted)
        if (isAllPermissionsGranted) {
            viewState.startCamera(CameraOptions())
        } else {
            viewState.showPermissionRequiredErrorMsg()
        }
    }

    fun onPermissionControlBtnClicked() {
        isPermissionCheckRequired = true
        viewState.showAppPermissionsScreen()
    }

    fun onScreenStarted() {
        if (permissionsRepo.isPermissionGranted(requiredPermissions)) {
            viewState.startCamera(CameraOptions())
            viewState.setTakePhotoBtnVisible(true)
        } else {
            viewState.setTakePhotoBtnVisible(false)
            if(isPermissionCheckRequired) {
                viewState.showPermissionRequiredErrorMsg()
            } else {
                viewState.requestPermissions(requiredPermissions)
            }
        }
        isPermissionCheckRequired = false
    }

    fun onCameraOpened() {
        viewState.setProgressVisible(false)
    }

    fun onSwipeDetected(swipeDirection: SwipeDirection) {
        val filter =  when (swipeDirection) {
            SwipeDirection.RIGHT -> cameraFiltersRepo.getPreviousFilter()
            else -> cameraFiltersRepo.getNextFilter()
        }
        viewState.setEffect(filter)
    }

    fun onTakePictureClicked() {
        viewState.takePicture()
    }

    fun picturePictureTaken(result: PictureResult) {
        disposables += photoRepository.savePhoto(result)
            .subscribe({
                viewState.showMessage(R.string.camera_photo_saved)
            }, {
                viewState.showMessage(R.string.camera_photo_save_error)
                Timber.e(it)
            })
    }

}