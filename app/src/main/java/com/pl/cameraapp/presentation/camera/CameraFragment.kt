package com.pl.cameraapp.presentation.camera

import android.content.Intent
import android.net.Uri.fromParts
import android.os.Bundle
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pl.cameraapp.*
import com.pl.cameraapp.data.camera.CameraFilterType
import com.pl.cameraapp.presentation.base.BaseFragment
import com.pl.cameraapp.presentation.base.model.CameraOptions
import com.pl.cameraapp.ui.animation.TapAnimationListener
import com.pl.cameraapp.ui.camera.CameraFiltersFactory
import com.pl.cameraapp.ui.camera.SimpleCameraListener
import com.pl.cameraapp.ui.swipeHandling.SwipeLeftRightListener
import kotlinx.android.synthetic.main.fr_camera.*
import javax.inject.Inject


class CameraFragment : BaseFragment(), CameraMvpView {

    companion object {
        private const val PERMISSION_REQUEST_KEY = 101
    }

    override val layoutId = R.layout.fr_camera

    @Inject
    @InjectPresenter
    lateinit var presenter: CameraPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        CameraApp.component.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        permissionsControlBtnTextView.setOnClickListener { presenter.onPermissionControlBtnClicked() }
        with(takePhotoBtnImageView) {
            setOnClickListener { presenter.onTakePictureClicked() }
            setOnTouchListener(TapAnimationListener())
        }
        cameraView.addCameraListener(SimpleCameraListener(onCameraOpened = {
            presenter.onCameraOpened()
        }, onPictureTaken = {
            presenter.picturePictureTaken(it)
        }))
    }

    override fun onStart() {
        super.onStart()
        presenter.onScreenStarted()
    }

    override fun onStop() {
        super.onStop()
        cameraView.close()
    }

    override fun onDestroyView() {
        cameraView.destroy()
        super.onDestroyView()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_KEY) {
            presenter.onPermissionsGranted(grantResults)
        }
    }

    override fun requestPermissions(permissions: Array<String>) {
        requestPermissions(permissions, PERMISSION_REQUEST_KEY)
    }

    override fun startCamera(cameraOptions: CameraOptions) {
        cameraView.setOnTouchListener(SwipeLeftRightListener(requireContext()) {
            presenter.onSwipeDetected(it)
        })
        permissionRequiredErrorMsgLinearLayout.hide()
        cameraProgressBar.setVisible(true)
        cameraView.open()
        cameraView.show()
    }

    override fun showPermissionRequiredErrorMsg() {
        permissionRequiredErrorMsgLinearLayout.show()
        cameraView.setOnTouchListener(null)
        cameraView.close()
        cameraView.hide()
    }

    override fun showAppPermissionsScreen() {
        startActivity(Intent(ACTION_APPLICATION_DETAILS_SETTINGS).apply {
            data = fromParts("package", requireContext().packageName, null)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        })
    }

    override fun setProgressVisible(visible: Boolean) {
        cameraProgressBar.setVisible(visible)
    }

    override fun setTakePhotoBtnVisible(visible: Boolean) {
        takePhotoBtnImageView.setVisible(visible)
    }

    override fun takePicture() {
        cameraView.takePictureSnapshot()
    }

    override fun setEffect(filterType: CameraFilterType) {
        cameraView.filter = CameraFiltersFactory.newFilter(filterType)
    }

    override fun showMessage(msgResId: Int) {
        requireContext().toast(getString(msgResId))
    }

}