package com.pl.cameraapp.presentation.camera

import androidx.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pl.cameraapp.data.camera.CameraFilterType
import com.pl.cameraapp.presentation.base.model.CameraOptions

interface CameraMvpView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun requestPermissions(permissions: Array<String>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAppPermissionsScreen()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(@StringRes msgResId: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun takePicture()

    fun startCamera(cameraOptions: CameraOptions)
    fun showPermissionRequiredErrorMsg()
    fun setProgressVisible(visible: Boolean)
    fun setEffect(filterType: CameraFilterType)
    fun setTakePhotoBtnVisible(visible: Boolean)
}