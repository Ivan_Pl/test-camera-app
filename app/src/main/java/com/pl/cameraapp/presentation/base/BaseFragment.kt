package com.pl.cameraapp.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pl.moxyandroidx.MvpAndroidXFragment
import timber.log.Timber

abstract class BaseFragment : MvpAndroidXFragment() {

    protected abstract val layoutId: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutId, container, false)
        view.isClickable = true
        return view
    }

    override fun onStart() {
        super.onStart()
        Timber.tag("Navigation").d("${this.javaClass.simpleName} started")
    }
}
