package com.pl.cameraapp.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pl.cameraapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
