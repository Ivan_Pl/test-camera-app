package com.pl.cameraapp.ui.animation

import android.view.MotionEvent
import android.view.View


class TapAnimationListener : View.OnTouchListener {

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            v.animate().cancel()
            v.animate().scaleXBy(0.5f).setDuration(300).start()
            v.animate().scaleYBy(0.5f).setDuration(300).start()
            return false
        } else if (event.action == MotionEvent.ACTION_UP) {
            v.animate().cancel()
            v.animate().scaleX(1f).setDuration(300).start()
            v.animate().scaleY(1f).setDuration(300).start()
            return false
        }
        return false
    }
}