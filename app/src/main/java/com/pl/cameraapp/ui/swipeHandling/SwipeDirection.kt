package com.pl.cameraapp.ui.swipeHandling

enum class SwipeDirection {
    LEFT, RIGHT
}