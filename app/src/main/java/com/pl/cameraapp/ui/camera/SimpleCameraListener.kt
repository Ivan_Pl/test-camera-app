package com.pl.cameraapp.ui.camera

import com.otaliastudios.cameraview.CameraException
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraOptions
import com.otaliastudios.cameraview.PictureResult

class SimpleCameraListener(
    private val onCameraOpened: (() -> Unit)? = null,
    private val onCameraClosed: (() -> Unit)? = null,
    private val onCameraError: ((exception: CameraException) -> Unit)? = null,
    private val onPictureTaken: ((result: PictureResult) -> Unit)? = null
) : CameraListener() {

    override fun onCameraOpened(options: CameraOptions) {
        super.onCameraOpened(options)
        onCameraOpened?.invoke()
    }

    override fun onCameraClosed() {
        super.onCameraClosed()
        onCameraClosed?.invoke()
    }

    override fun onCameraError(exception: CameraException) {
        super.onCameraError(exception)
        onCameraError?.invoke(exception)
    }

    override fun onPictureTaken(result: PictureResult) {
        super.onPictureTaken(result)
        onPictureTaken?.invoke(result)
    }

}