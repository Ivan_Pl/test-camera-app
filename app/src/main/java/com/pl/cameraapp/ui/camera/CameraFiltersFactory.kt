package com.pl.cameraapp.ui.camera

import com.otaliastudios.cameraview.filter.Filter
import com.otaliastudios.cameraview.filter.NoFilter
import com.otaliastudios.cameraview.filters.GrainFilter
import com.otaliastudios.cameraview.filters.InvertColorsFilter
import com.otaliastudios.cameraview.filters.SepiaFilter
import com.pl.cameraapp.data.camera.CameraFilterType

object CameraFiltersFactory {

    fun newFilter(filterTypeName: CameraFilterType): Filter {
        return when (filterTypeName) {
            CameraFilterType.EFFECT_GRAIN -> GrainFilter()
            CameraFilterType.EFFECT_NEGATIVE -> InvertColorsFilter()
            CameraFilterType.EFFECT_SEPIA -> SepiaFilter()
            else -> NoFilter()
        }
    }

}