package com.pl.cameraapp.ui.swipeHandling

import android.content.Context
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import timber.log.Timber
import kotlin.math.abs


class SwipeLeftRightListener(context: Context, private val onSwipeListener: (SwipeDirection) -> Unit) : View.OnTouchListener {

    companion object {
        private const val SWIPE_THRESHOLD = 100
        private const val SWIPE_VELOCITY_THRESHOLD = 100
    }

    private val gestureDetector: GestureDetector = GestureDetector(context, GestureListener())


    override fun onTouch(v: View, event: MotionEvent): Boolean {
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (abs(diffX) > abs(diffY)) {
                    if (abs(diffX) > SWIPE_THRESHOLD && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        val swipeDirection = if (diffX > 0) SwipeDirection.RIGHT else SwipeDirection.LEFT
                        Timber.v("On swipe detected $swipeDirection")
                        onSwipeListener.invoke(swipeDirection)
                        return true
                    }
                }
            } catch (exception: Exception) {
                Timber.e(exception)
            }
            return false
        }
    }

}