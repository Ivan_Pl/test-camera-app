package com.pl.cameraapp.data

import android.content.Context
import com.pl.cameraapp.isPermissionGranted
import javax.inject.Inject

class AppPermissionsRepository @Inject constructor(private val context: Context) {

    fun isPermissionGranted(permissions: Array<String>): Boolean {
        return permissions.none { !context.isPermissionGranted(it) }
    }

}