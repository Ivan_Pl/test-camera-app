package com.pl.cameraapp.data.camera

enum class CameraFilterType {
    NO_FILTER, EFFECT_GRAIN, EFFECT_NEGATIVE, EFFECT_SEPIA
}