package com.pl.cameraapp.data

import com.pl.cameraapp.data.camera.CameraFilterType
import javax.inject.Inject

class CameraFilterRepository @Inject constructor() {

    private var selectedFilter: CameraFilterType = CameraFilterType.NO_FILTER

    fun getCurrentFilter() = selectedFilter

    fun getPreviousFilter(): CameraFilterType {
        val currentFilterPosition = selectedFilter.ordinal
        return (if (currentFilterPosition != 0) {
            CameraFilterType.values()[currentFilterPosition - 1]
        } else {
            CameraFilterType.NO_FILTER
        }).also { selectedFilter = it }
    }

    fun getNextFilter(): CameraFilterType {
        val currentFilterPosition = selectedFilter.ordinal
        val lastFilterPosition = CameraFilterType.values().size - 1
        return (if (currentFilterPosition == lastFilterPosition) {
            CameraFilterType.values()[lastFilterPosition]
        } else {
            CameraFilterType.values()[currentFilterPosition + 1]
        }).also { selectedFilter = it }
    }

}