package com.pl.cameraapp.data

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import com.otaliastudios.cameraview.PictureResult
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class PhotoRepository @Inject constructor(private val context: Context) {

    fun savePhoto(result: PictureResult): Single<String> {
        return Single.create<String> { emitter ->
            try {
                val tempFile = createImageFile()
                val out = FileOutputStream(tempFile)
                out.write(result.data)
                out.close()
                addPicToGallery(tempFile)
                emitter.onSuccess(tempFile.absolutePath)
            } catch (exception: Exception) {
                emitter.onError(exception)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Calendar.getInstance().time)
        val storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera")
        if (!storageDir.exists())
            storageDir.mkdirs()
        return File.createTempFile(timeStamp, ".jpeg", storageDir)
    }

    private fun addPicToGallery(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        context.sendBroadcast(mediaScanIntent)
    }

}